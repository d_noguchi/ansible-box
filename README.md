vagrant 上で ansible を実行するためのスタートキット
===================================================

動作環境
--------

- VirtualBox 4.3.10
- Vagrant 1.5.3
- ホストOS  
Fedora 19
- ゲストOS  
CentOS 6.5  
使用した box : https://github.com/2creatives/vagrant-centos/releases/download/v6.5.1/centos65-x86_64-20131205.box

client から server へ ssh するために
------------------------------------

```bash
$ vagrant ssh-config client > ssh_config
$ vagrant ssh-config server >> ssh_config
$ scp -F ssh_config ~/.vagrant.d/insecure_private_key client:.ssh/id_rsa
$ scp -F ssh_config ./config client:.ssh/config
```

